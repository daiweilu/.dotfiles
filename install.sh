# install homebrew
ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

brew install bash-completion git hub vim maven pyenv fish node wget tree

# Syntax highlight cat
pip install Pygments

# setup fish as default shell
chsh -s /usr/local/bin/fish

# setup autojump for fish
git clone https://github.com/joelthelion/autojump.git ~/.autojump
