function gcloud-container-get-cred -d 'Get google cloud cluster credentials for kubectl'
  gcloud container clusters list | sel 1 | gxargs gcloud container clusters get-credentials
end
