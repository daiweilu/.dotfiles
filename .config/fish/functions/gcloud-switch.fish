function gcloud-switch -d 'gcloud switch configurations'
  gcloud config configurations list | sel 1 | gxargs gcloud config configurations activate
end
