function export_dlite_docker_host -d 'set DOCKER_HOST env with dlite'
  set -xg DOCKER_HOST 'tcp://'(dlite ip)
  echo $DOCKER_HOST
end
