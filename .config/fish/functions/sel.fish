function sel
  fzf -m --header-lines=1 | awk '{print $'$argv[1]'}'
end
