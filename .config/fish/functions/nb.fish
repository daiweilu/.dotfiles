function nb -d 'Node with --harmony_destructuring --harmony_default_parameters'
  node --harmony_destructuring --harmony_default_parameters $argv
end
