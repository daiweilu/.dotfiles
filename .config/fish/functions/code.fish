function code -d 'Launch MS Code Editor'
  env VSCODE_CWD="$PWD" open -n -b "com.microsoft.VSCode" --args $argv
end
