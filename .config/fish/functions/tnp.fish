function tnp -d 'tmux split-window and run command'
  tmux split-window -v $argv
end
