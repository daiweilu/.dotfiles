function _vf_prompt
  if set -q VIRTUAL_ENV
    echo -n -s (set_color -b blue white) (basename "$VIRTUAL_ENV") (set_color normal)
  end
end

function fish_right_prompt
  set_color blue
  _vf_prompt
  set_color normal
  echo ' '
  set_color red
  echo $RUBY_VERSION
  set_color normal

  if test $NVM_BIN
    echo ' '
    set_color green

    if test (echo $NVM_BIN | cut -d'/' -f5) = 'versions'
      echo (echo $NVM_BIN | cut -d'/' -f6)'-'(echo $NVM_BIN | cut -d'/' -f7)
    else
      echo 'node-'
      echo $NVM_BIN | cut -d'/' -f5
    end

    set_color normal
  end

end
