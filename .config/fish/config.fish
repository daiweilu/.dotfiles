# Path to Oh My Fish install.
set -gx OMF_PATH /Users/daiwei/.local/share/omf

# Customize Oh My Fish configuration path.
set -gx OMF_CONFIG /Users/daiwei/.config/omf

source $OMF_CONFIG/before.fish

# Load oh-my-fish configuration.
source $OMF_PATH/init.fish
