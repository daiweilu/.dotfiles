set -xg NVM_DIR $HOME/.nvm
# set -xg JAVA_HOME (/usr/libexec/java_home -v 1.8)
set -xg GOPATH $HOME/.gopath
set -xg PATH $HOME/bin $PATH ./node_modules/.bin $GOPATH/bin
set -xg DVM_DIR $HOME/.dvm

if not test $TMUXSPLIT
  bass source /Users/daiwei/.rvm/scripts/rvm
  bass source /Users/daiwei/google-cloud-sdk/path.bash.inc
  bass source /Users/daiwei/google-cloud-sdk/completion.bash.inc

  eval (python -m virtualfish compat_aliases auto_activation)
end

if not test $TMUX
  tas
end
